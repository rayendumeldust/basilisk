
PACKAGENAME=basilisk
venv=pipenv run
docs=docs/
VERSION=$(shell $(venv) python basilisk/version.py)

.PHONY: build install uninstall
build:
	$(venv) python setup.py sdist bdist_wheel
install: build
	$(venv) pip install dist/$(PACKAGENAME)-$(VERSION)-py3-none-any.whl
uninstall:
	$(venv) pip uninstall basilisk

.PHONY: docs
docs:
	$(venv) make -C $(docs) html

.PHONY: clean
clean:
	rm -rf docs/_build/
	rm -rf dist/


#!/usr/bin/env python3

'''
Manage the peripheral devices around the laptop.
'''

import subprocess
import shlex
import argparse
import os

from . import display

name = 'basilisk'

hp_display = {
    'name': 'hp',
    'os_name': 'HDMI2',
    'nickname': ['hp'],
    'priority': 100,
    'mode': '1920x1200',
    'sys_name': 'card0-HDMI-A-2',
    'orientation': '--left-of eDP1'
}
displaytest = {
    'name': 'test',
    'os_name': 'HDMI2',
    'nickname': ['hp'],
    'priority': 50,
    'mode': '1920x1200',
    'sys_name': 'card0-HDMI-A-2',
    'orientation': '--left-of eDP1'
}

sys_display_path = '/sys/class/drm/'


def get_all_displays():
    '''Get all available displays'''
    display_folders = os.listdir(sys_display_path)

    # sort out default devices
    display_filter = ['version', 'card0', 'renderD128']
    for item in display_filter:
        display_folders.remove(item)
    return display_folders


def get_display_status(displayname):
    '''Get the status of the display'''
    status_file_path = os.path.join(sys_display_path, displayname, 'status')

    with open(status_file_path) as f:
        status = f.read()

    status = status.rstrip()

    return status


class Interface(object):
    '''This class describes a peripheral interface.'''
    def __init__(self, name, os_name):
        '''Init function'''
        self.name = name
        # OS internal name
        self.os_name = os_name

    def enable_interface(self):
        '''Runs the interface command'''
        print(self.command)

    def __str__(self):
        return self.name


class InterfaceList(list):
    '''This class extends the list class to search inside the list for a
    device.'''

    def get_interface_by_name(self, name):
        '''Get the interface by name'''

        device = [x for x in self if x.name == name]
        if device is None:
            return None
        else:
            return device[0]

    def get_interface_by_priority(self, name):
        '''Get the interface with the highest priority'''
        sorted_priorities = sorted(self, key=lambda k: k.priority)
        # return the last item
        return sorted_priorities[-1]


class DisplayInterface(Interface):
    '''Class for display interfaces'''

    @classmethod
    def create_from_dictionary(cls, dictionary):
        '''This is a constructor to create a display interface from a
        dictionary.'''

        new_class = cls(dictionary.get('name'), dictionary.get('os_name'))
        new_class.set_mode(dictionary.get('mode'))
        new_class.set_orientation(dictionary.get('orientation'))
        new_class.set_sys_name(dictionary.get('sys_name'))
        new_class.set_priority(dictionary.get('priority'))

        return new_class

    def set_sys_name(self, sys_name):
        '''Set the sys name of the device'''
        self.sys_name = sys_name
        return

    def set_priority(self, priority):
        '''Set the sys name of the device'''
        self.priority = priority
        return

    def set_mode(self, mode):
        '''set the resultion mode of the screen'''
        self.mode = mode
        return

    def set_orientation(self, orientation):
        '''set the orientation of the screen'''
        self.orientation = orientation
        return

    def use_interface(self):
        '''Run whatever command to use the interface.'''

        self.check_display()

        command = "xrandr --output {} --mode {} {}".format(self.os_name,
                                                           self.mode,
                                                           self.orientation)
        result = subprocess.run(shlex.split(command))
        if result.returncode == 0:
            print('Set Display Interface to {} was successfull'.
                  format(self.name))
        return

    def check_display(self):
        '''Check if the display interface is available.

        The status file reads connected or disconnected and sits at following
        location:
            /sys/class/drm/card0-HDMI-A-2/status

        '''

        status_file_path = '/sys/class/drm/{}/status'.format(self.sys_name)
        with open(status_file_path) as f:
            status = f.read()

        if status == 'connected':
            return True
        else:
            return False


def parse_commandline_arguments():
    '''parse command line arguments'''

    parser = argparse.ArgumentParser()
    parser.add_argument("--display", '-d', action='store_true',
                        help="Set the display interface")
    parser.add_argument("--audio", '-a', type=str,
                        help="Set the audio interface")

    args = parser.parse_args()
    return args


def create_display_list():
    '''create and return a list of all displays'''

    display_list = InterfaceList()
    display_list.append(DisplayInterface.create_from_dictionary(hp_display))
    display_list.append(DisplayInterface.create_from_dictionary(displaytest))

    return display_list


def set_display(display):
    '''set the display'''

    # create a list of all attached devices
    display_list = create_display_list()

    # find the device
    if display == 'auto':
        interface = display_list.get_interface_by_priority(display)
    else:
        interface = display_list.get_interface_by_name(display)

    # edge case for auto, find the device with the highest priority
    # set display interface
    if interface:
        interface.use_interface()
    else:
        print('Something went wrong')

    return


def set_audio_device(audio_device='auto'):
    '''set the default audio device'''

    # check if audio device is available TODO
    focusrite_name = 'alsa_output.usb-Focusrite_Scarlett_2i2_USB-00.\
analog-stereo'

    # set the audio device
    # command = 'pacmd set-default-sink "{}"'.format(audio_devices[audio_device].pulse_name)
    command = 'pacmd set-default-sink "{}"'.format(focusrite_name)
    print(command)
    subprocess.check_call(shlex.split(command))
    print('Audio Device: set to {}'.format(focusrite_name))
    return


def testfunction():
    pass


def main():
    '''main function'''

    args = parse_commandline_arguments()

    if args.audio:
        print('set audio interface')  # TODO

    if args.display:
        display.search_physical_displays()
        # set_display(args.display)
    # set display
    # set_screen(args.screen)

    # set audio device
    # set_audio_device(args.audio_device)
    # set_audio_device()


if __name__ == '__main__':
    main()

import collections
import subprocess
import re
import logging
import logging.config

logger = logging.getLogger('basilisk.' + __name__)


class SoundCards(collections.UserList):
    """This class holds all the soundcards as a dictionary."""

    def __init__(data):
        collections.UserList.__init__(data)


class SoundCard(collections.UserDict):
    """This class holds all sound cards as a dictionary.

    :param dict data: Initial data, can also be None.
    """

    def __init__(data):
        collections.UserDict.__init__(data)

    def is_present(self):
        """Returns if the display is connected."""

        if self.data['resolution']:
            return True
        else:
            return False

    def is_connected(self):
        """Returns if the display is connected."""

        if self.data['status'] == 'connected':
            return True
        else:
            return False

    def activate(self):
        """Activate the display."""

        # TODO use sink name
        command = ['pacmd', 'set-default-sink', self.name]
        subprocess.run(command, stdout=subprocess.PIPE)
        logger.debug(command)
        return
    # TODO mute  pacmd set-sink-mute alsa_output.usb-Focusrite_Scarlett_2i2_USB-00.analog-stereo 0

    @property
    def name(self):
        """
        """
        return self.data['name']

    @property
    def sink(self):
        """
        """
        pass


def show_all_audio_interfaces():
    """Function to show all audio interfaces.
    """
    command = ['pacmd', 'list-cards']
    process_output = subprocess.run(command, stdout=subprocess.PIPE)
    # convert to utf8
    output = process_output.stdout.decode('utf8')
    lines = output.split('\n')

    regex = re.compile(r"\tname:\s<(?P<name>[a-zA-Z0-9].*)>")  # Screen name
    interface_names = SoundCards()
    for line in lines:
        result = regex.search(line)
        if result:
            result_dict = result.groupdict()
            soundcard = SoundCard()
            for key in result_dict:
                soundcard[key] = result_dict[key]
            print(soundcard.name)
            interface_names.append(soundcard)
    return interface_names


def activate_scarlett():
    """
    """
    command = ['pacmd', 'set-default-sink', 'alsa_output.usb-Focusrite_Scarlett_2i2_USB-00.analog-stereo']
    subprocess.run(command, stdout=subprocess.PIPE)
    return

"""
Module to configure basilisk.
"""

import logging
import logging.config


def setup_logging(stdout=True, logfile=False, level='DEBUG'):
    """Setup the logging for the application.

    :param bool stdout: Print log messages to standard output.
    :param bool logfile: Write log message to a log file.
    :param str level: Set logging level.
    """

    # TODO add a handler for logfiles
    # setup the loglevel
    # timestamps?

    logging_dict = {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'standard': {
                'format': '%(asctime)s [%(levelname)s] %(name)s: %(message)s'
            },
            'minimal': {
                'format': '[%(levelname)s] %(name)s: %(message)s'
            },
        },
        'handlers': {
            'default': {
                'level': 'DEBUG',
                'class': 'logging.StreamHandler',
                'formatter': 'minimal'
            },
        },
        'loggers': {
            'root': {
                'handlers': ['default'],
                'level': 'DEBUG',
                'propagate': True
            },
            'basilisk': {
                'handlers': ['default'],
                'level': level,
                'propagate': True
            }
        }
    }

    logging.config.dictConfig(logging_dict)

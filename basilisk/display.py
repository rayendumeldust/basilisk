"""
Module for display things.
"""

import subprocess
import re
import collections
import logging
import logging.config

from . import config

logger = logging.getLogger('basilisk.' + __name__)

xrandr = ['xrandr']


class Displays(collections.UserList):
    """This class holds all the displays as a dictionary."""

    def __init__(data):
        collections.UserList.__init__(data)

    def get_all_names(self):
        """Return all display names."""
        names = [x['name'] for x in self.data]
        return names

    def get_display(self, name):
        """Return display dictionary.

        :param str name: System name of the display.
        """
        names = [x for x in self.data if x['name'] == name]
        if len(names) > 1:
            raise Exception('Multiple displays with the same name.')
        elif len(names) == 0:
            return None
        else:
            return names[0]


class Display(collections.UserDict):
    """This class holds all the displays as a dictionary.

    :param dict data: Initial data, can also be None.
    """

    def __init__(data):
        collections.UserDict.__init__(data)

    def is_activated(self):
        """Returns if the display is connected."""

        if self.data['resolution']:
            return True
        else:
            return False

    def is_connected(self):
        """Returns if the display is connected."""

        if self.data['status'] == 'connected':
            return True
        else:
            return False

    def activate(self):
        """Activate the display."""

        # Check if it is activated
        if self.is_activated() is True:
            logger.error('Display is already activated.')
            return
        elif self.is_connected() is False:
            logger.error('Display is not connected.')
            return

        command = ['xrandr', '--output', self.data['name'], '--left-of', 'eDP1', '--mode', ' 2560x1440']
        subprocess.run(command, stdout=subprocess.PIPE)
        logger.debug(command)
        return


def search_displays():
    """An easier method to search for displays.
    """
    regex = re.compile(r"(?P<name>[\-A-Za-z0-9]+)\s(?P<status>connected|disconnected)\s(?P<resolution>[0-9\+x]+){0,1}")  # Screen name
    process_output = subprocess.run(xrandr, stdout=subprocess.PIPE)
    output_text = process_output.stdout.decode('utf-8')
    display_matches = list(regex.finditer(output_text))
    display_dict = [x.groupdict() for x in display_matches]
    return display_dict


def activate_display(display):
    """A very basic method to activate a display.
    """
    if display['status'] != 'connected':
        raise Exception('Display is not connected')

    command = ['xrandr', '--output', display['name'], '--left-of', 'eDP1', '--mode', '2560x1440']
    subprocess.run(command, stdout=subprocess.PIPE)
    return


def search_physical_displays():
    """Search for all physically available displays."""
    process_output = subprocess.run(xrandr, stdout=subprocess.PIPE)
    # decode the output
    output_text = process_output.stdout.decode('utf-8')

    devices = parse_xrandr_output(output_text)
    return devices


def parse_xrandr_output(output_text):
    """Parse the xrandr output.

    :param str output_text: Output of the xrandr command.
    """
    devices = Displays()
    for line in output_text.splitlines():
        device = parse_xrandr_line(line)
        # only append if a display was found
        if device:
            userdisplay = Display()
            for key in device:
                userdisplay[key] = device[key]
            devices.append(userdisplay)
            logger.debug("Display '{}' found.".format(userdisplay['name']))

    return devices


def parse_xrandr_line(line):
    """Search for display devices and return all device information.

    :param str line: One individual line of the xrandr output.
    """

    regex = re.compile(r"(?P<name>[\-A-Za-z0-1]+)\s(?P<status>connected|disconnected)\s(?P<resolution>[0-9\+x]+){0,1}")  # Screen name
    result = regex.search(line)
    if result:
        return result.groupdict()
    else:
        return None


def show_all_displays():
    """Show all displays."""
    return


def set_display_state():
    """Set a display state."""
    return


def main():
    """
    """

    config.setup_logging()

    displays = search_displays()
    names = [x['name'] for x in displays]

    logger.debug(f"Found displays: {', '.join(names)}")

    # get Dell display
    filtered_displays = [x for x in displays if x['name'] == 'DP1']
    if len(filtered_displays) == 0:
        logger.error('Display can not be found')
        return
    dell = filtered_displays[0]
    if dell:
        if dell['status'] == 'connected':
            activate_display(dell)


if __name__ == '__main__':
    main()

#!/usr/bin/env python

import collections
import subprocess
import shlex


def system_commands():
    """
    """
    commands = collections.OrderedDict()
    commands['lock'] = 'i3lock -e -i "/home/freddy/Pictures/wallpaper/lockscreen-schloss.png"'
    commands['shutdown'] = 'shutdown -h now'
    commands['suspend'] = 'sudo pm-suspend'
    return commands


def run_command(command):
    """
    """
    command_list = shlex.split(command)
    subprocess.run(command_list)
    return


def run_rofi(options):
    option_string = '\n'.join(options)
    rofi_command = 'rofi -dmenu'
    rofi_command_list = shlex.split(rofi_command)
    output = subprocess.run(rofi_command_list, stdout=subprocess.PIPE, input=option_string.encode())
    option = output.stdout.decode('utf-8').rstrip()
    if option not in options:
        raise ValueError
    return option


def main():
    # run the dialoge for system commands
    commands = system_commands()
    option = run_rofi(commands)
    run_command(commands[option])
    return


if __name__ == '__main__':
    main()

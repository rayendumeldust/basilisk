
"""
Wallpapers are manager using a well known file path: ~/.config/wallpaper

The normal way is to create a symlink to the file.

    ln -s ~/Pictures/wallpaper/ryan-waring-366060-unsplash.jpg ~/.config/wallpaper
    feh --bg-scale ~/.config/wallpaper
"""

import argparse
import subprocess
from pathlib import Path
import logging
import os
import pathlib
import sys

from . import config

logger = logging.getLogger('basilisk.' + __name__)

home = str(Path.home())
wallpaper_symlink = home + '/.config/wallpaper'


def load_wallpaper():
    """Load the current wallpaper at ~/.config/wallpaper
    """
    command = ['feh', '--bg-scale', wallpaper_symlink]
    subprocess.run(command, stdout=subprocess.PIPE)
    logger.info(f'Successfully loaded wallpaper to {wallpaper_symlink}')
    return


def set_wallpaper_link(path):
    """Set a new wallpaper link to ~/.config/wallpaper
    """
    if not os.path.exists(path):
        raise ValueError(f"The file '{path}' does not exist")
    os.unlink(wallpaper_symlink)
    os.symlink(path, wallpaper_symlink)
    logger.info(f"Succesfully set wallpaper symlink to '{path}'")
    return


def parse_args(args=sys.argv[1:]):
    parser = argparse.ArgumentParser(description='Interact with wallpapers')
    parser.add_argument('--path', required=False, type=pathlib.Path,
                        help='Path to the new wallpaper')

    return parser.parse_args(args)


def main():
    config.setup_logging()
    args = parse_args()

    if args.path:
        set_wallpaper_link(args.path)
    load_wallpaper()
    return


if __name__ == '__main__':
    main()

.. basilisk documentation master file, created by
   sphinx-quickstart on Sat Sep  8 20:26:58 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to basilisk's documentation!
====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Goals
-----

* One goal should be that one is able to turn on and of screens in xrandr
  It should look similar to this::

    basilisk --display hp off
    basilisk --display list
    basilisk --display interactive




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

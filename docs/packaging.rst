Packaging
=========


https://packaging.python.org/tutorials/packaging-projects/

https://packaging.python.org/guides/single-sourcing-package-version/

Version
-------

The version of the package is described in *basilisk/version.py*

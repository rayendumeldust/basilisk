import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="basilisk",
    version_format='{tag}.{commitcount}+{gitsha}',
    author="Freddy",
    author_email="frederic.a.becker@googlemail.com",
    description="Linux interface control",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="",
    packages=setuptools.find_packages(),
    entry_points={
        'console_scripts': [
            'foo = basilisk.version:main',
            'basilisk-display = basilisk.display:main',
            'basilisk-system = basilisk.system:main',
            'basilisk-scarlett = basilisk.audio:activate_scarlett',
            'basilisk-wallpaper = basilisk.wallpaper:main',
        ],
    }
)
